﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts
{
    public class ResolutionSceneControllerScript : MonoBehaviour
    {
        [SerializeField] private Text _resolutionText;
        [SerializeField] private GameObject _secondTestButton;
        [SerializeField] private GameObject _quitButton;
        private void Start()
        {
            if(PlayerPrefs.GetInt("QuestionIndex") < 22)
            {
                _resolutionText.text = AnswersContainer.GetResolution();
                if(AnswersContainer.SecondTest)
                {
                    _secondTestButton.SetActive(true);
                }
            }

            else
            {
                _secondTestButton.SetActive(false);
                _quitButton.SetActive(true);
                _resolutionText.text = AnswersContainer.GetResolutionSecTest();
            }
        }

        public void OnBackToStartButtonClick()
        {
            Debug.Log("BackToSTart");
            PlayerPrefs.DeleteAll();
            AnswersContainer.SecondTest = false;
            SceneManager.LoadScene(1);
        }

        public void OnSecondaryTestButtonClick()
        {
            SceneManager.LoadScene(2);
            PlayerPrefs.SetInt("QuestionIndex", 21);
        }

    }
}