﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Scripts
{
    public class TestSceneControllerScript : MonoBehaviour
    {
        [SerializeField] private Sprite[] _ishiharaSheetSprites;
        [SerializeField] private Image _sceneCurrentImage;
        [SerializeField] private Dropdown _optionsDropdown;
        [SerializeField] private Text _dropDownDefaultText;
        [SerializeField] private Animator _dropDownAnim;
        private bool _isOptionSelected;
        private const string DropDownDefaultText = "Escull una opció";
        private bool _isLoadingNextScene;
        private readonly string[][] _optionsArrays = new string[][] { 
            new string[] { "12", "12", "12" }, 
            new string[] { "8", "3", "No puc veure res" }, 
            new string[] { "6", "5", "No puc veure res" },
            new string[] { "29", "70", "No puc veure res" },
            new string[] { "57", "35", "No puc veure res" },
            new string[] { "5", "2", "No puc veure res" },
            new string[] { "3", "5", "No puc veure res" },
            new string[] { "15", "17", "No puc veure res" },
            new string[] { "74", "21", "No puc veure res" },
            new string[] { "2", "No puc veure res", "No puc veure res" },
            new string[] { "6", "No puc veure res", "No puc veure res" },
            new string[] { "97", "No puc veure res", "No puc veure res" },
            new string[] { "45", "No puc veure res", "No puc veure res" },
            new string[] { "5", "No puc veure res", "No puc veure res" },
            new string[] { "7", "No puc veure res", "No puc veure res" },
            new string[] { "16", "No puc veure res", "No puc veure res" },
            new string[] { "73", "No puc veure res", "No puc veure res" },
            new string[] { "No puc veure res", "5", "No puc veure res" },
            new string[] { "No puc veure res", "2", "No puc veure res" },
            new string[] { "No puc veure res", "45", "No puc veure res" },
            new string[] { "No puc veure res", "73", "No puc veure res" },
            new string[] { "26", "6", "2" },
            new string[] { "42", "2", "4" },
            new string[] { "35", "5", "3" },
            new string[] { "96", "6", "9" }
        };

        [SerializeField] private Text _finalitzarTestButton;

        private void Start()
        {
            _sceneCurrentImage.sprite = _ishiharaSheetSprites[PlayerPrefs.GetInt("QuestionIndex")];
            NewDropDown(_optionsArrays[PlayerPrefs.GetInt("QuestionIndex")]);
        }

        public void OnNextButtonClick()
        {
            if(!AnswersContainer.SecondTest)
            {
                if (PlayerPrefs.GetInt("QuestionIndex") < 20)
                {
                   
                    if (_isOptionSelected)
                    {
                        _isLoadingNextScene = true;
                        ProcessAnswer();
                        //var selectedOption = _optionsDropdown.options[_optionsDropdown.value].text;
                        PlayerPrefs.SetInt("QuestionIndex", PlayerPrefs.GetInt("QuestionIndex") + 1);
                        SceneManager.LoadScene(2);
                    }

                    else
                    {
                        _dropDownAnim.SetBool("isFlickering", true);
                        StartCoroutine(DisableAnimatorAfterAnim());
                    }
                }

                else
                {
                    ProcessAnswer();
                    SceneManager.LoadScene(3);
                }

                
            }

            else
            {
                if (PlayerPrefs.GetInt("QuestionIndex") < _ishiharaSheetSprites.Length - 1)
                {
                    _isLoadingNextScene = true;
                    ProcessAnswerSecondTest();
                    PlayerPrefs.SetInt("QuestionIndex", PlayerPrefs.GetInt("QuestionIndex") + 1);
                    SceneManager.LoadScene(2);
                }

                else
                {
                    _isLoadingNextScene = true;
                    ProcessAnswerSecondTest();
                    SceneManager.LoadScene(3);
                }
            }
        }

        public void OnBackButtonClick()
        {
            if (PlayerPrefs.GetInt("QuestionIndex") > 0)
            {
                _isLoadingNextScene = true;
                PlayerPrefs.SetInt("QuestionIndex", PlayerPrefs.GetInt("QuestionIndex") - 1);
                SceneManager.LoadScene(2);
            }

            else
            {
                SceneManager.LoadScene(1);
            }
        }

        void NewDropDown(string[] newDDArrray)
        {
            _dropDownDefaultText.text = DropDownDefaultText;
            _optionsDropdown.options.Clear();
            for(int i = 0; i < newDDArrray.Length; i++)
            {
                _optionsDropdown.options.Add(new Dropdown.OptionData(newDDArrray[i]));
            }
        }

        private void OnDisable()
        {
            if(!_isLoadingNextScene)
            {
                PlayerPrefs.SetInt("QuestionIndex", 0);
            }
        }

        public enum MultipleAnswersChoice
        {
            Correct,
            RedGreenBlindness,
            TotalBlindness
        }

        private void ProcessAnswer()
        {
            switch(_optionsDropdown.value)
            {
                case 0:
                    AnswersContainer.SetAnswer(PlayerPrefs.GetInt("QuestionIndex"), AnswersContainer.MultipleAnswersChoice.Correct);
                    break;
                case 1:
                    AnswersContainer.SetAnswer(PlayerPrefs.GetInt("QuestionIndex"), AnswersContainer.MultipleAnswersChoice.RedGreenBlindness);
                    break;
                case 2:
                    AnswersContainer.SetAnswer(PlayerPrefs.GetInt("QuestionIndex"), AnswersContainer.MultipleAnswersChoice.TotalBlindness);
                    break;
            }

            Debug.Log(PlayerPrefs.GetInt("QuestionIndex") + "||" + AnswersContainer._answersArray[PlayerPrefs.GetInt("QuestionIndex")]);
        }

        private void ProcessAnswerSecondTest()
        {
            switch (_optionsDropdown.value)
            {
                case 0:
                    AnswersContainer.SetSecTestAnswer(PlayerPrefs.GetInt("QuestionIndex"), AnswersContainer.MultipleAnswersChoiceSecondTest.Correct);
                    break;
                case 1:
                    AnswersContainer.SetSecTestAnswer(PlayerPrefs.GetInt("QuestionIndex"), AnswersContainer.MultipleAnswersChoiceSecondTest.Protanopy);
                    break;
                case 2:
                    AnswersContainer.SetSecTestAnswer(PlayerPrefs.GetInt("QuestionIndex"), AnswersContainer.MultipleAnswersChoiceSecondTest.Tritanopy);
                    break;
            }

            Debug.Log(PlayerPrefs.GetInt("QuestionIndex") + "||" + AnswersContainer._answersArraySecondTest[PlayerPrefs.GetInt("QuestionIndex")]);
        }

        public void SelectedOptionControl()
        {
            _isOptionSelected = true;
        }

        IEnumerator DisableAnimatorAfterAnim()
        {
            yield return new WaitForSeconds(0.9f);
            _dropDownAnim.SetBool("isFlickering", false);
        }

        private void Update()
        {
            if (PlayerPrefs.GetInt("QuestionIndex") == 20)
            {
                _finalitzarTestButton.text = "Finalitzar test";
            }
        }
    }
}
