﻿using UnityEngine;

namespace Scripts
{
    public static class AnswersContainer
    {
        public static MultipleAnswersChoice[] _answersArray = new MultipleAnswersChoice[21];
        public static MultipleAnswersChoiceSecondTest[] _answersArraySecondTest = new MultipleAnswersChoiceSecondTest[25];
        public static int _correctAnswersCount;
        public static int _redGreenBlindnessCount;
        public static int _totalBlindnessCount;
        public static int _correctAnswersCountSecondTest;
        public static int _protanopyCount;
        public static int _tritanopyCount;
        private readonly static string _correctResolution = "La teva visió es correcta, tot i això, si tens dubtes et recomanem visitar un professional";
        private readonly static string _redGreenResolution = "Hem detectat deficiències per diferenciar entre tons de verd-vermell (tricomacia anòmala). Si ho desitges, pots realitzar un test secundari per esbrinar el tipus de daltonisme que pateixes";
        private readonly static string _totalBlindnessResolution = "Hem detectat ceguera total al color o debilitat cromàtica, et recomanem encaridament que visitis un professional";
        private readonly static string _undefinedResolution = "No hem pogut detectar si pateixes daltonisme o no, et recomanem visitar un professional";
        private readonly static string _undefinedResolutionSecTest = "No hem pogut esbrinar el tipus de tricomacia anòmala que pateixes, et recomanem visitar un professional";
        private readonly static string _protanopyResolution = "Probablement pateixes de protanopia, et recomanem visitar un professional per assegurar el diagnòstic";
        private readonly static string _tritanopyResolution = "Probablement pateixes de tritanopia, et recomanem visitar un professional per assegurar el diagnòstic";
        private static bool _secondTest;
        public static bool SecondTest { get => _secondTest; set => _secondTest = value; }


        public enum MultipleAnswersChoice
        {
            Correct,
            RedGreenBlindness,
            TotalBlindness
        }

        public enum MultipleAnswersChoiceSecondTest
        {
            Correct,
            Protanopy,
            Tritanopy
        }

        public static void SetAnswer(int index, MultipleAnswersChoice answer)
        {
            _answersArray[index] = answer;
        }

        public static void SetSecTestAnswer(int index, MultipleAnswersChoiceSecondTest answer)
        {
            _answersArraySecondTest[index] = answer;
        }

        private static void GetFinalCount()
        {

            foreach (var answer in _answersArray)
            {
                switch (answer)
                {
                    case MultipleAnswersChoice.Correct:
                        _correctAnswersCount++;
                        break;
                    case MultipleAnswersChoice.RedGreenBlindness:
                        _redGreenBlindnessCount++;
                        break;
                    case MultipleAnswersChoice.TotalBlindness:
                        _totalBlindnessCount++;
                        break;
                }
            }

            Debug.Log("Correct answers: " + _correctAnswersCount);
            Debug.Log("Red green blindness answers: " + _redGreenBlindnessCount);
            Debug.Log("Total blindness answers: " + _totalBlindnessCount);
        }

        private static void GetFinalCountSecTest()
        {

            foreach (var answer in _answersArraySecondTest)
            {
                switch (answer)
                {
                    case MultipleAnswersChoiceSecondTest.Correct:
                        _correctAnswersCountSecondTest++;
                        break;
                    case MultipleAnswersChoiceSecondTest.Protanopy:
                        _protanopyCount++;
                        break;
                    case MultipleAnswersChoiceSecondTest.Tritanopy:
                        _tritanopyCount++;
                        break;
                }
            }
            _correctAnswersCountSecondTest -= 21;
            Debug.Log("Correct answers: " + _correctAnswersCountSecondTest);
            Debug.Log("Red green blindness answers: " + _protanopyCount);
            Debug.Log("Total blindness answers: " + _tritanopyCount);
        }

        public static string GetResolution()
        {
            GetFinalCount();

            if(_correctAnswersCount > _redGreenBlindnessCount && _correctAnswersCount > _totalBlindnessCount)
            {
                return _correctResolution;
            }

            else if(_redGreenBlindnessCount > _correctAnswersCount && _redGreenBlindnessCount > _totalBlindnessCount)
            {
                SecondTest = true;
                return _redGreenResolution;
            }

            else if(_totalBlindnessCount > _correctAnswersCount && _totalBlindnessCount > _redGreenBlindnessCount)
            {
                return _totalBlindnessResolution;
            }

            else
            {
                return _undefinedResolution;
            }
        }

        public static string GetResolutionSecTest()
        {
            GetFinalCountSecTest();

            if (_correctAnswersCountSecondTest > _protanopyCount && _correctAnswersCountSecondTest > _tritanopyCount)
            {
                return _undefinedResolutionSecTest;
            }

            else if (_protanopyCount > _correctAnswersCountSecondTest && _protanopyCount > _tritanopyCount)
            {
                return _protanopyResolution;
            }

            else if (_tritanopyCount > _correctAnswersCountSecondTest && _tritanopyCount > _protanopyCount)
            {
                return _tritanopyResolution;
            }

            else
            {
                return _undefinedResolutionSecTest;
            }
        }
    }
}