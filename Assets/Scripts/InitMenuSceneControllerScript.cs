﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts
{
    public class InitMenuSceneControllerScript : MonoBehaviour
    {
        public void OnQuitButtonClick()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
        }

        public void OnInitButtonClick()
        {
            PlayerPrefs.SetInt("QuestionIndex", 0);
            SceneManager.LoadScene(2);
        }
    }
}